import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.css']
})
export class ContentComponent implements OnInit {

  results: any[];
  constructor(private http: HttpClient){
  }
  ngOnInit(): void {
    this.http.get('http://dev.studio31.co/api/get_recent_posts/').subscribe(data => {
      this.results = data["posts"];
    });
  }

}
